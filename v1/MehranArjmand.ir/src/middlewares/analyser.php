<?php
namespace Mehranarji\App\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;

class Analyser
{
    /**
     * @var mixed
     */
    protected $context;

    /**
     * @param $c
     */
    public function __construct($c)
    {
        $this->context = $c;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $agent     = $request->getHeader('user_agent')[0];
        $ipAddress = $request->getAttribute('ip_address');

        // $this->context->logger->info($agent);
        // $this->context->logger->info($ipAddress);

        $request = $request->withAttribute('agent', $agent);
        return $next($request, $response);
    }
}
