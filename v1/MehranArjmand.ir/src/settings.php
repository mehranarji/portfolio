<?php
return [
    'settings' => [
        'version' => '1.0.0',
        
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Website Identity
        'identity' => [
            'title' => 'Mehran Arjmand'
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // View settings
        'view' => [
            'cache' => false,
            'templates' => __DIR__ . '/../templates/',
        ],

        'db' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'mehranarji',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ]
    ],
];
