<?php
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Mehranarji\App\Middleware\Auth;
use Mehranarji\App\Middleware\Analyser;
use Mehranarji\App\Model\People;
use Mehranarji\App\Controller;

// Middleware
$checkProxyHeaders  = true; // Note: Never trust the IP address for security processes!
$trustedProxies     = []; // Note: Never trust the IP address for security processes!

$app->add(new Analyser($app->getContainer()));
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));

// Routes
$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    // $this->logger->info($request);
    // Render index view

    // $this->db;

    // $people = new People();
    // $people->name = "majid";
    // $people->save();

    return $this->view->render($response, 'index.twig', $args);
})->add(new Auth);


$app->get('/soldiership', function (Request $request, Response $response, array $args) {
    // Sample log message
    // $this->logger->info($request);
    // $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->view->render($response, 'soldiership.twig', ['title' => 'Soldiership']);
})->add(new Auth);

$app->get('/review', function (Request $request, Response $response, array $args) {
    // Sample log message
    // $this->logger->info($request);
    // $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->view->render($response, 'review.twig', ['title' => 'Reviews']);
})->add(new Auth);

$app->group('/api/v1', function (App $app) {
    $app->get('/people', function (Request $request, Response $response, array $args) {
        $this->db;
        $peoples = People::all();

        return $response->withJson($peoples);
    });

    $app->post('/review', Controller\Review::class . ':add');
});
