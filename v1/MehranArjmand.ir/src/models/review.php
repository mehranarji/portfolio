<?php namespace Mehranarji\App\Model;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
  protected $table = "reviews";

  public function people()
  {
    return $this->belongsTo('Mehranarji\App\Model\People');
  }
}
