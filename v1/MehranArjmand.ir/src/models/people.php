<?php namespace Mehranarji\App\Model;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
  protected $table    = "peoples";
  protected $guarded  = ['id'];
  protected $hidden   = ['id'];

  public function review()
  {
    return $this->hasOne('Mehranarji\App\Model\Review');
  }
}
