<?php namespace Mehranarji\App\Model;

class Result
{
  private const SUCCESS_FIELD = "success";
  
  public function __construct()
  {
    # code...
  }

  public static function resolve($message = null, $data = null)
  {
    return [ Self::SUCCESS_FIELD => true, 'message' => $message, 'data' => $data ];
  }
  
  public static function reject($message)
  {
    return [ Self::SUCCESS_FIELD => false, 'message' => $message ];
  }

  public static function success($message, $data)
  {
    return Self::resolve($message, $data);
  }
  
  public static function error($message)
  {
    return Self::reject($message);
  }
  
}
