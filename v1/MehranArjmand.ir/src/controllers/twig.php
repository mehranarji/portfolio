<?php namespace Mehranarji\App\Controller;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MyTwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{

  protected $settings;

	function __construct($settings)
	{
		$this->settings = $settings;
	}

  
  public function getFilters()
  {
    return [
      new \Twig_SimpleFilter('versionize', [$this, 'versionizer']),
      new \Twig_SimpleFilter('v', [$this, 'versionizer'])
    ];
  }

  public function getGlobals()
  {
    return [
    ];
  }

  public function versionizer($address)
	{
		return sprintf('%s?v=%s',$address, $this->settings['version']);
	}

  
}

?>