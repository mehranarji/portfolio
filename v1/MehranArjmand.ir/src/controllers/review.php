<?php namespace Mehranarji\App\Controller;

Use Slim\Http\Request;
Use Slim\Http\Response;
use Mehranarji\App\Model;
use Mehranarji\App\Model\Result;
use \Psr\Http\Message\UploadedFileInterface;

class Review extends Base
{
  public function add(Request $request, Response $response, $args) {
    $params = $request->getParsedBody();
    
    $people = new Model\People();
    $people->name = $params['name'];
    $people->save();
    
    $review = new Model\Review();
    $review->text = $params['message'];
    $review->rate = $params['rate'];
    $people->review()->save($review);

    $files = $request->getUploadedFiles();
    $files['avatar']->moveTo(__DIR__ . '/tst.png');

    return $response->withJson(Result::resolve());
  }
}
