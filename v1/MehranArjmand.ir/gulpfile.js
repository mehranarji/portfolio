"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const browser = require("browser-sync").create();
const sourcemaps = require("gulp-sourcemaps");
const usemin = require("gulp-usemin");
const uglify = require("gulp-uglify");
const rev = require("gulp-rev");
const cleanCss = require("gulp-clean-css");
const clean = require("gulp-clean");
const zip = require("gulp-zip");
const fs = require("fs");
const pug = require("gulp-pug");
const util = require("gulp-util");
const plumber = require("gulp-plumber");

sass.compiler = require("node-sass");

const init = function(cb) {
  browser.init({
    open: false,
    // reloadDelay: 0,
    port: 3007,
    proxy: '127.0.0.1:8080'
  });

  cb();
};

const watch = function(cb) {
  gulp.watch("public_html/assets/styles/**/*.scss", style);
  // gulp.watch(
  //   ["css/panel/*.css", "css/panel/style.css", "!css/panel/rtl.css"],
  //   css
  // );
  // gulp.watch(["js/**/*.js"], reload);
  // gulp.watch(["templates/**/*.pug"], views);
  // gulp.watch(["*.html", "!*-en.html"], reload);
  gulp.watch(["**/*.php", "!vendor/**"], reload);

  cb();
};

const reload = function(cb) {
  browser.reload();

  cb();
};

const style = function(cb) {
  gulp
    .src("public_html/assets/styles/style.scss")
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "compact" }).on("error", sass.logError))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("public_html/assets/styles"))
    .pipe(browser.stream({ match: "**/*.css" }));

  cb();
};

const css = function(cb) {
  gulp.src("css/panel/*.css").pipe(browser.stream());

  cb();
};

const views = function(cb) {
  return gulp
    .src("templates/*-en.pug")
    .pipe(plumber())
    .pipe(
      pug({
        pretty: true
      })
    )
    .pipe(plumber.stop())
    .pipe(gulp.dest("./"))
    .pipe(browser.stream({ match: "**/*.html", once: true }));

  cb();
};

exports.default = gulp.series([init, watch]);
exports.style = style;
