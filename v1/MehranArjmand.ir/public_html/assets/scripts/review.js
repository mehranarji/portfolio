
$(function () {
  var uploader = new qq.FineUploaderBasic({
    request: {
      endpoint: '/api/v1/review',
      inputName: 'avatar'
    },
    button: $('.avatar .upload-btn').get(0),
    debug: false,
    multiple: false,
    autoUpload: false,
    validation: {
      acceptFiles: "image/*",
      stopOnFirstInvalidFile: false,
    },

    form: {
      element: $('.review-container form').get(0),
      interceptSubmit: true
    },
    callbacks: {
      onSubmitted: function (res) {
        this.drawThumbnail(res, $('.avatar img').get(0), 150, false);
        AvatarAdded();
      }    
    }
  });

  function AvatarAdded() {
    // $('.avatar img').show();
    $('.avatar').addClass('not-empty');
  }

  function AvatarRemove() {
    $('.avatar img').hide();
    $('.avatar .upload-btn').show();
  }

  
});
