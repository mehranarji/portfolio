'use strict';

$(function () {

  var DImageLoad, DOverlay, DNameAnime, DSocialAnime, DloopAnime;

  var $parent = $('.main-title');
  var chars = $parent.text().split("");
  $('.main-title').empty();

  for (var i = 0; i < chars.length; i++) {
    var $node = $('<span>');
    if (chars[i] == ' ') {
      $node.text(chars[i]);
      $node.addClass('space');
    } else
      $node.text(chars[i]);
    $parent.append($node);
  }

  DImageLoad = $('body').imagesLoaded({
    background: true
  });

  DOverlay = anime({
    targets: '.overlay',
    duration: 1500,
    elasticity: 0,
    delay: 500,
    // opacity: [1, 0],
    // top: 100,
    autoplay: false,
    begin : function (anim) {
      $(anim.animatables[0].target).addClass('hide');
    },
    complete: function (anim) {
      $(anim.animatables[0].target).hide();
    }
  });

  DNameAnime = anime({
    targets: '.main-title span',
    duration: 500,
    elasticity: 0,
    delay: function (el, i, l) {
      return i * 10 + 0;
    },
    translateX: [-150, 0],
    opacity: [0, 1],
    scale: [.8, 1],
    autoplay: false
  });

  DSocialAnime = anime({
    targets: '.socials a',
    duration: 500,
    elasticity: 0,
    delay: function (el, i, l) {
      return i * 10;
    },
    translateY: [-50, 0],
    opacity: [0, 1],
    scale: [.8, 1],
    autoplay: false
  });

  DloopAnime = anime({
    loop: true,
    targets: '.main-title span',
    duration: 1000,
    easing: 'easeInOutQuart',
    direction: 'normal',
    elasticity: 0,
    delay: function (el, i, l) {
      return i * 20 + 15000;
    },
    translateY: [
      { value: 0 },
      { value: -15 },
      { value: 0 },
    ],
    autoplay: false
  });

  DImageLoad.done(DOverlay.play);
  DOverlay.finished.then(DNameAnime.play);
  DNameAnime.finished.then(DSocialAnime.play);
  DSocialAnime.finished.then(DloopAnime.play);
})