'use strict';

$(function () {
  
  var start_date  = moment('2018-02-20 00:00').utcOffset("+03:30"),
    end_date      = moment('2019-11-22 00:00').utcOffset("+03:30"),
    now           = moment().utcOffset("+03:30"),
    remain        = end_date.diff(now, 'days'),
    total         = end_date.diff(start_date, 'days'),
    percent       = Math.max(0, ((total - remain) * 100) / total);
    
  $('.container.d-none').removeClass('d-none');

  if ( remain >= 0 ) {
    $('.remain-days').text(remain);
  } else {
    $('.remain-days').text( -remain);
    $('.desc').text('Days Passed');
  }

  var DNumberEnter, DProgress, DProgressBar;

  DNumberEnter = anime({
    targets: '.remain-days, .desc, .footnote',
    duration: 500,
    elasticity: 0,
    delay: function (el, i, l) {
      return i * 100 + 0;
    },
    translateX: [-150, 0],
    opacity: [0, 1],
    scale: [.8, 1],
    autoplay: false
  });

  DProgress = anime({
    targets: '.progress',
    duration: 500,
    delay: 0,
    elasticity: 0,
    translateX: [-100, 0],
    width: ['0%', '100%'],
    autoplay: false
  });

  DProgressBar = anime({
    targets: '.progress-bar',
    duration: 500,
    delay: 0,
    elasticity: 0,
    width: ['0%', percent + '%'],
    autoplay: false
  });

  setInterval(function () {
    DNumberEnter.play();
    DNumberEnter.finished.then(DProgress.play);
    DProgress.finished.then(DProgressBar.play);
  }, 2000);

  particlesJS('particles', {
    "particles": {
      "number": {
        "value": Math.max(remain, 0),
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 1,
        "random": true,
        "anim": {
          "enable": true,
          "speed": 1,
          "opacity_min": 0,
          "sync": false
        }
      },
      "size": {
        "value": 3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 4,
          "size_min": 0.3,
          "sync": false
        }
      },
      "line_linked": {
        "enable": false,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 1,
        "direction": "none",
        "random": true,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 600
        }
      }
    },
    "interactivity": {
      "detect_on": "window",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "bubble"
        },
        "onclick": {
          "enable": false,
          "mode": "repulse"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 250,
          "size": 0,
          "duration": 2,
          "opacity": 0,
          "speed": 3
        },
        "repulse": {
          "distance": 400,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  });

});