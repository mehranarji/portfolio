# [__Mehran Arjmand__](https://mehranarjmand.ir/) Portfolio
Hi, My name is mehran and i'm a software engineer from _Iran_

My talent
- HTML5
- CSS3
- Stylus
- JavaScript (+jquery)
- AngularJs
- Gulp
- C++
- Java
- Android
- Wordpress

---

## __Web__ Projects


### [Baran Charity Group Official Website](http://barang.ir/)
- Theme Design
- Front-end Development
- Theme Development ([Wordpress](http://wordpress.org/))
- Responsive

### [Baran Charity Group Portal](http://panel.barang.ir/)
- Front-end Development
- Responsive

### [Payam Noor E-Library](http://www.pnueb.com/)
- Front-end Development Based On Pre Available Theme ([Bootstrap](http://getbootstrap.com/))

### [Gigamiga Client area](https://app-il.gigamiga.com/clientarea.php)
- Bases on [Whmcs](https://www.whmcs.com/)

### [Rassam Funds landing page](http://mehranarji.gitlab.io/rasam-landing/)
- Front-end Development
- Responsive

### [Mohammad Hossein Sarvghadi Official Website](http://sarvghadi.ir/)
- Theme Development ([Wordpress](http://wordpress.org/))
- Front-end Development ([Bootstrap](http://getbootstrap.com/))
- Responsive

### [Duologik Solutions](http://www.duologik.com/)
- Theme Development ([Wordpress](http://wordpress.org/))
- PDF Download Plugin Development 
- Responsive

### [Part Data Processing Official Website](http://partdp.ir/)
- Theme Development ([Wordpress](http://wordpress.org/))

### [Part Inc.](http://partinc.ir/)
- Front-end Development (HTML+CSS)
- Responsive

### [Rasam Funds](http://rasamfunds.com/)
- Front-end Development (HTML+CSS)

### [Funds Page embedded in Rasam](http://armaghanalmasfund.rasamfunds.com/) (the link is just an example, this website use dynamic subdomain and content)
- Front-end Development (HTML+CSS)

### Bourse Khan
- Front-end Development (HTML+CSS)
- Responsive

### [Rahavard Modiriat](http://rahavard.ir/)
- Some improvments on their theme

### [Motalebehgari](http://www.motalebehgari.ir/)
- Theme Development ([Wordpress](http://wordpress.org/))

### GDL Solutions Inc. _(previous version)_
- Theme Development ([Wordpress](http://wordpress.org/))

### Dr.Ousia _(previous version)_
- Theme Development ([Wordpress](http://wordpress.org/))

### [Sadr Novin Khorasan](http://sadrtrading.com/)
- Front-end Development
- Theme Development ([Wordpress](http://wordpress.org/)) with custom Post and categories

### [Khavaran Alyaf](http://khavaranalyaf.com/)
- Theme Development ([Wordpress](http://wordpress.org/))

### [Zanganeh Lawyers](https://zanganeh.org/)
- Theme Design
- Theme Development ([Wordpress](http://wordpress.org/))
- Front-end Development
- Responsive

### [Stock Trade](https://trade.rasamfunds.com/)
- Proudly Front-end development with stylus

### [Ketabchin](https://www.ketabchin.com/)
- Responsive improvments
- UI improvments

### [Ketabchin Landing](https://www.ketabchin.com/landing/)
- Front-end Development
- Responsive

### [Signal Website](http://signal724.ir/)
- Theme Development ([Wordpress](http://wordpress.org/))

### [Sakku Blog](https://blog.sakku.cloud/)
- Theme Development ([Wordpress](http://wordpress.org/))

### [FusedBone Blog and App](https://blog.fusedbone.com/)
- Theme Development ([Wordpress](http://wordpress.org/))
- Wordpress plugin over ZBS


---

<!-- ## __Design__ Projects -->
<!-- ## Posters -->
<!-- ## Photographs -->


## My __Friends__
- **Mehran Ganji KH**
    - He is my BFF and working on electronic devices, we have some major contribution.

- **Seyyed Mostafa HosseiniDB**
    - Photographer, Director, Front-end developer & Amazing Friend.

- **[Omid Keveh Rad](http://kavehrad.ir/)**
    - One of best design artist ever (in my opinion)

- **Hossein Kiani**
    - My First inspiration in Web development, Good exprience in Php Developing
    
- **[Nasser Torabzadeh](http://hyperblog.ir/)**
    - Serious and professional [NodeJS](https://nodejs.org/) developer.

- **[Majid Yaghoti](http://majid.yaghouti.com/)**
    - another good coworker and perfect [NodeJS](https://nodejs.org/) developer


- **[Naser Ashrafi](http://www.nasserashrafi.com/)**
    - a Good friend and advisor and great designer

- **[Elyas Ghasemi](http://elyas74.blog.ir/)**
    - He is really insane and crazy but loyal friend, Nodejs Developer

## __Contact__ Me
- [Email Me](mailto://mehranarji@gmail.com)
- Find me on [Github](https://github.com/MehranArji/) and [Gitlab](https://gitlab.com/mehranarji/)
- Or follow me on [Linkedin](https://www.linkedin.com/in/mehran-arjmand-8b769a9a/), [Twitter](https://twitter.com/MehranArji/), [Instagram](https://www.instagram.com/mehranarji/), [Facebook](https://www.facebook.com/mehran.arjmand.33)
- Also online on [Telegram Messenger](http://t.me/mehranarji/)
- Checkout my music taste on [Lastfm](https://www.last.fm/user/MehranArji/)

